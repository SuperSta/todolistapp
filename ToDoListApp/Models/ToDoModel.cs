﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ToDoListApp.Models
{
    class ToDoModel: INotifyPropertyChanged
    {
       
        private bool _isDone;
        private string _toDo;

        public DateTime TimeOfCreation { get; set; } = DateTime.Now;

        public bool IsDone
        {
            get { return _isDone; }
            set 
            {
                if (_isDone == value)
                    return;
           
                _isDone = value;
                OnPropertyChanged("IsDone");
            }
        }

        public string ToDo
        {
            get { return _toDo; }
            set 
            { 
               if (_toDo == value)
                    return;

                _toDo = value;
                OnPropertyChanged("ToDo");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string PropertyName = "")
        {
           
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
